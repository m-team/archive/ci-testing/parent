# Triggering pipelines

See <https://codebase.helmholtz.cloud/m-team/archive/ci-testing/parent>
for an example.

Triggering pipelines (in the community edition (CE)) is available via:
```yaml
include:
  - 'https://codebase.helmholtz.cloud/m-team/tools/ci-voodoo/raw/master/ci-include/parent-child.yml'
```
This include is already there, if you use `.../ci-include/generic-ci.yml`.

You need a `TRIGGER_TOKEN`: for the triggered (child) project. 
    1. Create it: in the child project under "settings -> ci/cd -> Pipeline trigger tokens"
    2. Name it like the parent project
    3. In the parent project undder "settings -> ci/cd -> Variables" store
       it at "TRIGGER_TOKEN_<TRIGGERED_PROJECT_NAME>"
       - You **MUST** MASK the token!

## Usage
```yaml
  script:
    - !reference [.def-trigger-pipeline]
    - trigger_pipeline --project-name ${DWNSTRM_PROJECT_NAME} \
                       --branch ${TRIGGER_BRANCH} \
                       --trigger-token ${TRIGGER_TOKEN} \
                       --<ANY_OTHER_VARIABLE_NAME> "That you need in the triggered pipeline"
     # Returns: TRIGGERED_PIPELINE_ID
```
